
$(".btn-modal").fancybox({
    'padding'    : 0,
    baseTpl:
    '<div class="fancybox-container fancybox-container-white" role="dialog" tabindex="-1">' +
    '<div class="fancybox-bg"></div>' +
    '<div class="fancybox-inner">' +
    '<div class="fancybox-infobar">' +
    "<span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span>" +
    "</div>" +
    '<div class="fancybox-toolbar">{{buttons}}</div>' +
    '<div class="fancybox-navigation">{{arrows}}</div>' +
    '<div class="fancybox-stage"></div>' +
    '<div class="fancybox-caption"></div>' +
    "</div>" +
    "</div>"
});

var portfolio = new Swiper('.portfolio-slider', {
    effect: 'cube',
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction'
    },
    navigation: {
        nextEl: '.portfolio-button-next',
        prevEl: '.portfolio-button-prev'
    }
});


$(function($){
    var wrap = $('.header__top');
    $h = 25;

    $(window).scroll(function(){

        if ( $(window).scrollTop() > $h) {
            wrap.addClass('fix-top');
        }
        else{
            wrap.removeClass('fix-top');
        }
    });
});



var review = new Swiper('.review-slider', {

    slidesPerView: 2,
    spaceBetween: 20,
    pagination: {
        el: '.swiper-pagination',
        type: 'fraction'
    },
    navigation: {
        nextEl: '.review-button-next',
        prevEl: '.review-button-prev'
    },
    breakpoints: {

        1300: {
            spaceBetween: 20
        },
        992: {
            slidesPerView: 1,
            spaceBetween: 10
        }
    }
});



$(document).ready(function() {

    $(".errors__video")
        .mouseenter(function() {
            this.play();
        })
    //     .mouseleave(function(){
    //       this.pause();
    // });

});




(function() {

    $('.chart_elem')
        .mouseenter(function() {
            var value = $(this).attr("data-value");
            var label = $(this).attr("data-label");
            var box = $(this).closest('.graph');

            box.find('.legend').addClass('hover');
            box.find('.legend__value').text(value);
            box.find('.legend__text').text(label);
        })
        .mouseleave(function(){

        });

    $('.graph')
        .mouseleave(function(){

            var box = $(this);
            box.find('.legend').removeClass('hover');
            box.find('.legend__value').empty();
        });
}());


(function() {

    $('.diagram__info li')
        .mouseenter(function() {
            var tab = '.' + $(this).attr("data-value");
            var box = $(this).closest('.diagram');
            var value = $(tab).attr("data-value");
            var label = $(tab).attr("data-label");

            box.find(tab).addClass('hover');
            box.find('.legend').addClass('hover');
            box.find('.legend__value').text(value);
            box.find('.legend__text').text(label);

            console.log(value);
            console.log(label);
        })
        .mouseleave(function(){
            var box = $(this).closest('.diagram');
            var tab = '.' + $(this).attr("data-value");
            box.find(tab).removeClass('hover');
            box.find('.legend').removeClass('hover');
            box.find('.legend__value').empty();
        });
}());

AOS.init({
    easing: 'ease-in-out-sine',
    disable: "mobile"
});


jQuery(document).ready(function(){
    jQuery('.scrollbar-inner').scrollbar();
});